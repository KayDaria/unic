import Promise     from 'bluebird';

import MongoClient from './mongoDB';
import logger      from './logger';
import config      from '../config.default';
import server      from './koa';

let mongoClient;

export function start() {
  process.on('uncaughtException', (err) => {
    logger.error({ source: 'uncaughtException', error: err.stack });

    process.exit(1);
  });

  process.on('unhandledRejection', (err) => {
    logger.error({ source: 'unhandledRejection', error: err.stack });

    process.exit(1);
  });

  mongoClient = new MongoClient(logger);

  return Promise.resolve(mongoClient.connect(config.mongoUri))
    .then(() => {
      server.start(logger, config.port);
      logger.info('Application started');
    })
    .catch(err => {
      logger.error({ source: 'uncaughtException', error: err.stack });

      process.exit(1);
    });
}

export function stop() {
  return Promise.all([
    server.stop(),
    mongoClient.disconnect()
  ])
    .then(() => logger.info('Application stopped'));
}

