import mongoose from 'mongoose';

const dataSchema = new mongoose.Schema({

}, {
  strict:     false,
  versionKey: false,
  timestamps: true,
  collection: 'data'
});

dataSchema.index({ 'expireAt': 1 }, { expires: '0s' });


module.exports.Data = mongoose.model('Data', dataSchema);
