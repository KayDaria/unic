import glob from 'glob';
import path from 'path';


function init() {
  glob.sync(`${__dirname}/*.js`)
    .forEach((file) => {
      require(path.resolve(file));
    });
}

module.exports = init;