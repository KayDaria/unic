import mongoose      from 'mongoose';

mongoose.Promise = Promise;

class MongoClient {
  constructor(logger) {
    this.logger = logger;

    this._initModels();
  }

  connect(uri) {
    const events = [
      { name: 'connecting',    logLevel: 'info' },
      { name: 'connected',     logLevel: 'info' },
      { name: 'open',          logLevel: 'info' },
      { name: 'disconnecting', logLevel: 'warn' },
      { name: 'disconnected',  logLevel: 'warn' },
      { name: 'close',         logLevel: 'warn' },
      { name: 'reconnected',   logLevel: 'warn' },
      { name: 'error',         logLevel: 'error' },
      { name: 'fullsetup',     logLevel: 'info' },
      { name: 'all',           logLevel: 'info' }
    ];

    events.forEach((event) => {
      mongoose.connection.on(event.name, (...args) => {
        const msg = {
          event:             'mongodb',
          mongooseEvent:     event.name,
          mongooseEventArgs: args
        };

        this.logger[event.logLevel](msg);
      });
    });

    return new Promise((resolve, reject) => {
      mongoose.connect(uri, {}, (err) => {
        if (err) return reject(err);

        return resolve(mongoose.connection);
      });
    });
  }

  disconnect() {
    return mongoose.disconnect();
  }

  _initModels() {
    require('./models')();
  }
}

export default MongoClient;
