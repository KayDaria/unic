import koa             from 'koa';
import bodyParser      from 'koa-bodyparser';
import Router          from 'koa-router';

let logger;
let server;

function start(_logger, port) {
  logger = _logger;

  let app = new koa();

  app.use(bodyParser());

  const router = new Router();
  router.post('/api/data', require('./dataCtrl').checkData);

  app.use(router.routes());
  app.use(router.allowedMethods());

  app.use(async (ctx, next) => {
    try {
      await next();
    } catch (e) {
      ctx.throw(500, `Unexpected Error: ${e.message}`);
    }
  });

  app.use(async (ctx, next) => {
    await next();

    if (ctx.status !== 404) return null;

    return ctx.throw(404, `Unable to resolve the request "${req.originalUrl}".`);
  });

  server = app.listen(port, (err) => {
    return new Promise((resolve, reject) => {
      if (err) return reject(err);

      logger.debug(`Server started at ${port} port`);
      return resolve();
    });
  });
}

function stop() {
  return new Promise((resolve, reject) => {
    server.close((err, result) => {
      return err ? reject(err) : resolve(result);
    });
  });
}

module.exports = {
  start,
  stop
};
