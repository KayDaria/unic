import mongoose   from 'mongoose';
import objectHash from 'object-hash';
import moment     from 'moment';

const Data = mongoose.model('Data');

function checkData(ctx) {
  const data = ctx.request.body;

  if (!data.ttl) return ctx.throw(400, 'Data doesn\'t contain ttl');

  const hash = objectHash(data);

  data.requestTime = moment().toISOString(true);
  data.hash        = hash;
  data.expireAt    = moment().add(data.ttl, 'seconds').toISOString(true);

  return Data.findOne({ hash }).count()
    .then(entry => {
      let promiseSave = null;

      if (!entry) promiseSave = Data.create(data);

      return Promise.resolve(promiseSave)
        .then(() => ctx.body = !entry)
    })
    .catch(err => ctx(500, err.message));
}

module.exports = {
  checkData
};